CREATE TABLE IF NOT EXISTS `matrix` (
                `row` TINYINT NOT NULL,
                `col` TINYINT NOT NULL,
                `value` INT,
                INDEX `x` (`col`),
                INDEX `y` (`row`),
                UNIQUE (`row`, `col`)
            ) ENGINE='InnoDB';

DELIMITER ;;
CREATE PROCEDURE `init_matrix` ()
BEGIN
                DECLARE i, row, col, val, flag, MATRIX_SIZE, matrix_cell_cout, init_count INT;
                SET i = 0, MATRIX_SIZE = 10, matrix_cell_cout = POW(MATRIX_SIZE, 2), init_count = matrix_cell_cout / 4;
                WHILE i < init_count DO
                    SET row = RAND() * (MATRIX_SIZE - 1);
                    SET col = RAND() * (MATRIX_SIZE - 1);
                    SET val = RAND() * 99999;
                    SET flag = 0;
                    SELECT COUNT(*) INTO flag FROM `matrix` WHERE `matrix`.`row` = row && `matrix`.`col` = col;
                    IF flag = 0 THEN
                        INSERT INTO matrix VALUES (row, col, val);
                    ELSE
                        SET i = i - 1;
                    END IF;
                    SET i = i + 1;
                END WHILE;
            END;;
DELIMITER ;