<?

class Widget
{
    protected
        $result,
        $defaultOptions = [
            'wrapper' => [
                'class' => 'wrapper',
                'tag' => 'ul'
            ],
            'item' => [
                'class' => 'default-item',
                'tag' => 'li'
            ]
        ];

    function __construct($data, $options = [])
    {
        $this->data = $data;
        $this->options = $options;

        $this->render();
    }

    function __set($name, $value)
    {
        $this->$name = is_callable($value)
            ? $this->$name = call_user_func($value)
            : $name == 'options'
                ? array_replace_recursive($this->defaultOptions, $value)
                : $value;
    }

    function __toString()
    {
        return $this->result;
    }

    protected function render()
    {
        $result = '';

        foreach ($this->data as $value)
        {
            $result .= $this->renderItem($value, $this->options['item']);
        }

        $result = $this->renderItem($result, $this->options['wrapper']);

        $this->result = $result;

        return $this;
    }

    protected function renderItem($content, $options)
    {
        $itemObj = $this->prepareItem($content, $options);

        $result = "<$itemObj->tag";

        foreach ($itemObj->attr as $name => $value)
        {
            $result .= " $name = '$value'";
        }

        $result .= ">$itemObj->content</$itemObj->tag>\n";

        return $result;
    }

    protected function prepareItem($data, $options)
    {
        $tag = $options['tag'];
        unset($options['tag']);

        if(isset($options['content']))
        {
            $content = $options['content'];
            unset($options['content']);
        }

        return (object)[
            'tag' => $tag,
            'content' => isset($content)
                            ? is_callable($content)
                                ? $this->renderContent($content, $data)
                                : $content
                            : $data,
            'attr' => $options
        ];
    }

    protected function renderContent($content, $data)
    {
        $data = is_array($data)
            ? (object) $data
            : $data;

        return call_user_func($content, $data, $this->data);
    }
}